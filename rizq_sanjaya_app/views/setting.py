from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from ..forms import *
from django.contrib import messages
from ..models import *
from django.utils import timezone
import os
import json
from ..decorators import *
from ..helpers import *
from datetime import datetime
from django.db import IntegrityError



    
@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_infografis(request):
    form = ''

    if request.method == 'GET':
        try: 
            deskripsi = RnW.objects.get(jenis = 'deskripsi')
        except: 
            deskripsi = ''
        try: 
            pertanyaan_1 = RnW.objects.get(jenis = 'pertanyaan_1')
        except: 
            pertanyaan_1 = ''
        try: 
            jawaban_1 = RnW.objects.get(jenis = 'jawaban_1')
        except: 
            jawaban_1 = ''
        try: 
            pertanyaan_2 = RnW.objects.get(jenis = 'pertanyaan_2')
        except: 
            pertanyaan_2 = ''
        try: 
            jawaban_2 = RnW.objects.get(jenis = 'jawaban_2')
        except: 
            jawaban_2 = ''
        try: 
            pertanyaan_3 = RnW.objects.get(jenis = 'pertanyaan_3')
        except: 
            pertanyaan_3 = ''
        try: 
            jawaban_3 = RnW.objects.get(jenis = 'jawaban_3')
        except: 
            jawaban_3 = ''
        try: 
            pertanyaan_4 = RnW.objects.get(jenis = 'pertanyaan_4')
        except: 
            pertanyaan_4 = ''
        try: 
            jawaban_4 = RnW.objects.get(jenis = 'jawaban_4')
        except: 
            jawaban_4 = ''
        try: 
            pertanyaan_5 = RnW.objects.get(jenis = 'pertanyaan_5')
        except: 
            pertanyaan_5 = ''
        try: 
            jawaban_5 = RnW.objects.get(jenis = 'jawaban_5')
        except: 
            jawaban_5 = ''

        try: 
            alamat = AppSetting.objects.get(nama = 'alamat')
        except: 
            alamat = ''
            
        try: 
            alamat_link = AppSetting.objects.get(nama = 'alamat_link')
        except: 
            alamat_link = ''

        try: 
            embed_maps = AppSetting.objects.get(nama = 'embed_maps')
        except: 
            embed_maps = ''

        try: 
            telepon = AppSetting.objects.get(nama = 'telepon')
        except: 
            telepon = ''

        try: 
            telepon_text = AppSetting.objects.get(nama = 'telepon_text')
        except: 
            telepon_text = ''

        try: 
            whatsapp = AppSetting.objects.get(nama = 'whatsapp')
        except: 
            whatsapp = ''

        try: 
            whatsapp_text = AppSetting.objects.get(nama = 'whatsapp_text')
        except: 
            whatsapp_text = ''

        try: 
            email = AppSetting.objects.get(nama = 'email')
        except: 
            email = ''

        try: 
            meta_property_title = AppSetting.objects.get(nama = 'meta_property_title')
        except: 
            meta_property_title = ''

        try: 
            meta_author = AppSetting.objects.get(nama = 'meta_author')
        except: 
            meta_author = ''

        try: 
            meta_keywords = AppSetting.objects.get(nama = 'meta_keywords')
        except: 
            meta_keywords = ''

        try: 
            meta_property_description = AppSetting.objects.get(nama = 'meta_property_description')
        except: 
            meta_property_description = ''

        try: 
            facebook = AppSetting.objects.get(nama = 'facebook')
        except: 
            facebook = ''

        try: 
            link_facebook = AppSetting.objects.get(nama = 'link_facebook')
        except: 
            link_facebook = ''

        try: 
            link_twitter = AppSetting.objects.get(nama = 'link_twitter')
        except: 
            link_twitter = ''

        try: 
            link_instagram = AppSetting.objects.get(nama = 'link_instagram')
        except: 
            link_instagram = ''

        try: 
            link_youtube = AppSetting.objects.get(nama = 'link_youtube')
        except: 
            link_youtube = ''

        try: 
            lazada = AppSetting.objects.get(nama = 'lazada')
        except: 
            lazada = ''

        try: 
            shopee = AppSetting.objects.get(nama = 'shopee')
        except: 
            shopee = ''

        try: 
            tokped = AppSetting.objects.get(nama = 'tokped')
        except: 
            tokped = ''

        try: 
            link_tiktok = AppSetting.objects.get(nama = 'link_tiktok')
        except: 
            link_tiktok = ''

        try: 
            header_layanan = AppSetting.objects.get(nama = 'header_layanan')
        except: 
            header_layanan = ''

        try: 
            header_galeri = AppSetting.objects.get(nama = 'header_galeri')
        except: 
            header_galeri = ''

        try: 
            header_kontak = AppSetting.objects.get(nama = 'header_kontak')
        except: 
            header_kontak = ''

        try: 
            foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
        except: 
            foto_diskon = ''

        try: 
            diskon = AppSetting.objects.get(nama = 'diskon')
        except: 
            diskon = ''

        try: 
            tanggal = AppSetting.objects.get(nama = 'tanggal')
            tanggal_lahir = tanggal.tanggal.strftime('%Y-%m-%d')
        except: 
            tanggal_lahir = ''

        # form = InfografisForm()
        context = {
            'title' : 'Infografis - Admin',
            'alamat' : alamat,
            'alamat_link' : alamat_link,
            'embed_maps' : embed_maps,
            'telepon' : telepon,
            'telepon_text' : telepon_text,
            'whatsapp' : whatsapp,
            'whatsapp_text' : whatsapp_text,
            'email' : email,
            'meta_property_title' : meta_property_title,
            'meta_author' : meta_author,
            'meta_property_description' : meta_property_description,
            'meta_keywords' : meta_keywords,
            'facebook' : facebook,
            'link_facebook' : link_facebook,
            'link_youtube' : link_youtube,
            'link_twitter' : link_twitter,
            'link_instagram' : link_instagram,
            'link_tiktok' : link_tiktok,
            'breadcrumb' : 'Infografis',
            'header_layanan' : header_layanan,
            'header_galeri' : header_galeri,
            'header_kontak' : header_kontak,
            'form' : form,
            'p1' : pertanyaan_1,
            'p2' : pertanyaan_2,
            'p3' : pertanyaan_3,
            'p4' : pertanyaan_4,
            'p5' : pertanyaan_5,
            'j1' : jawaban_1,
            'j2' : jawaban_2,
            'j3' : jawaban_3,
            'j4' : jawaban_4,
            'j5' : jawaban_5,
            'deskripsi' : deskripsi,
            'lazada' : lazada,
            'shopee' : shopee,
            'tokped' : tokped,
            'foto_diskon' : foto_diskon,
            'diskon' : diskon,
            'tanggal' : tanggal_lahir,
        }
        return render(request, 'profile/admin/setting/index.html', context)

    if request.method == 'POST':
        infografis = ''
        jenis = request.POST.get('jenis')
        form = ''
        if jenis == 'overview':
            try:
                infografis = AppSetting.objects.get(nama='alamat')
                infografis.keterangan = request.POST.get('alamat')
                infografis.save()
            except:
                infografis = AppSetting(nama='alamat', keterangan=request.POST.get('alamat'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='alamat_link')
                infografis.keterangan = request.POST.get('alamat_link')
                infografis.save()
            except:
                infografis = AppSetting(nama='alamat_link', keterangan=request.POST.get('alamat_link'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='embed_maps')
                infografis.keterangan = request.POST.get('embed_maps')
                infografis.save()
            except:
                infografis = AppSetting(nama='embed_maps', keterangan=request.POST.get('embed_maps'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='telepon_text')
                infografis.keterangan = request.POST.get('telepon_text')
                infografis.save()
            except:
                infografis = AppSetting(nama='telepon_text', keterangan=request.POST.get('telepon_text'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='telepon')
                infografis.keterangan = request.POST.get('telepon')
                infografis.save()
            except:
                infografis = AppSetting(nama='telepon', keterangan=request.POST.get('telepon'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='whatsapp')
                infografis.keterangan = request.POST.get('whatsapp')
                infografis.save()
            except:
                infografis = AppSetting(nama='whatsapp', keterangan=request.POST.get('whatsapp'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='whatsapp_text')
                infografis.keterangan = request.POST.get('whatsapp_text')
                infografis.save()
            except:
                infografis = AppSetting(nama='whatsapp_text', keterangan=request.POST.get('whatsapp_text'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='email')
                infografis.keterangan = request.POST.get('email')
                infografis.save()
            except:
                infografis = AppSetting(nama='email', keterangan=request.POST.get('email'))
                infografis.save()

        if jenis == 'sosmed':
            try:
                infografis = AppSetting.objects.get(nama='facebook')
                infografis.keterangan = request.POST.get('facebook')
                infografis.save()
            except:
                infografis = AppSetting(nama='facebook', keterangan=request.POST.get('facebook'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_facebook')
                infografis.keterangan = request.POST.get('link_facebook')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_facebook', keterangan=request.POST.get('link_facebook'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_twitter')
                infografis.keterangan = request.POST.get('link_twitter')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_twitter', keterangan=request.POST.get('link_twitter'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_youtube')
                infografis.keterangan = request.POST.get('link_youtube')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_youtube', keterangan=request.POST.get('link_youtube'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_instagram')
                infografis.keterangan = request.POST.get('link_instagram')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_instagram', keterangan=request.POST.get('link_instagram'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_tiktok')
                infografis.keterangan = request.POST.get('link_tiktok')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_tiktok', keterangan=request.POST.get('link_tiktok'))
                infografis.save()

        if jenis == 'ecomerce':
            try:
                infografis = AppSetting.objects.get(nama='lazada')
                infografis.keterangan = request.POST.get('lazada')
                infografis.save()
            except:
                infografis = AppSetting(nama='lazada', keterangan=request.POST.get('lazada'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='shopee')
                infografis.keterangan = request.POST.get('shopee')
                infografis.save()
            except:
                infografis = AppSetting(nama='shopee', keterangan=request.POST.get('shopee'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='tokped')
                infografis.keterangan = request.POST.get('tokped')
                infografis.save()
            except:
                infografis = AppSetting(nama='tokped', keterangan=request.POST.get('tokped'))
                infografis.save()

        if jenis == 'header':
            try:
                infografis = AppSetting.objects.get(nama='header_layanan')
                infografis.keterangan = request.POST.get('header_layanan')
                infografis.save()
            except:
                infografis = AppSetting(nama='header_layanan', keterangan=request.POST.get('header_layanan'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='header_galeri')
                infografis.keterangan = request.POST.get('header_galeri')
                infografis.save()
            except:
                infografis = AppSetting(nama='header_galeri', keterangan=request.POST.get('header_galeri'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='header_kontak')
                infografis.keterangan = request.POST.get('header_kontak')
                infografis.save()
            except:
                infografis = AppSetting(nama='header_kontak', keterangan=request.POST.get('header_kontak'))
                infografis.save()



        if jenis == 'meta':
            try:
                infografis = AppSetting.objects.get(nama='meta_property_title')
                infografis.keterangan = request.POST.get('meta_property_title')
                infografis.save()
            except:
                infografis = AppSetting(nama='meta_property_title', keterangan=request.POST.get('meta_property_title'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='meta_author')
                infografis.keterangan = request.POST.get('meta_author')
                infografis.save()
            except:
                infografis = AppSetting(nama='meta_author', keterangan=request.POST.get('meta_author'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='meta_keywords')
                infografis.keterangan = request.POST.get('meta_keywords')
                infografis.save()
            except:
                infografis = AppSetting(nama='meta_keywords', keterangan=request.POST.get('meta_keywords'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='meta_property_description')
                infografis.keterangan = request.POST.get('meta_property_description')
                infografis.save()
            except:
                infografis = AppSetting(nama='meta_property_description', keterangan=request.POST.get('meta_property_description'))
                infografis.save()

        if jenis == 'reseller':
            try: 
                image = request.FILES.get('image')
                if 'image' in request.FILES:
                    foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
                # print(image)
                # path_file_lama = f"{settings.MEDIA_ROOT}/{foto_diskon.image}"
                # os.remove(path_file_lama)
                # print(image)
                    foto_diskon.image = image
                    foto_diskon.save()
            except: 
                image = request.FILES.get('image')
                # print(image)
                foto_diskon = AppSetting(nama='foto_diskon', image=request.FILES.get('image'))
                foto_diskon.save()

            try:
                diskon = AppSetting.objects.get(nama='diskon')
                diskon.keterangan = request.POST.get('diskon')
                diskon.save()
            except:
                diskon = AppSetting(nama='diskon', keterangan=request.POST.get('diskon'))
                diskon.save()

            try:
                tanggal = AppSetting.objects.get(nama='tanggal')
                tanggal.tanggal = request.POST.get('tanggal')
                tanggal.save()
            except:
                tanggal = AppSetting(nama='tanggal', tanggal=request.POST.get('tanggal'))
                tanggal.save()

        messages.success(request, 'Informasi berhasil disimpan.')
        return redirect('profile:admin_setting_infografis')
        

