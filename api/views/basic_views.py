from django.shortcuts import render
# from merapitrip_app.models.profile import Carousel, AboutMe, Layanan, PaketWisata, PaketWisata_Detail, OTP, trx_order_list, trx_order_list_detail
from rest_framework import serializers
from rizq_sanjaya_app.models.profile import T_LRA_Jenis, T_LRA_Rekening
# from api.serializers import carousel_serializer as carousel
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db import IntegrityError
import pprint, uuid, json, base64, os
from django.db import transaction
from django.db.models import Sum
# from support import support_function as sup
from datetime import datetime, timedelta
from django.conf import settings
from rest_framework.permissions import IsAuthenticated

TRIWULAN = {'1': [1, 2, 3], '2': [4, 5, 6], '3': [7, 8, 9], '4': [10, 11, 12]}

class GetDataViewsets(APIView):
    permission_classes = (IsAuthenticated,)
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, jenis):
        tahun = request.GET.get('tahun')
        triwulan = request.GET.get('triwulan')

        data = {}

        if tahun is None or tahun == '':
            data['status'] = False
            data['message'] = 'Parameter tahun harus disertakan'
            return Response(data, status = status.HTTP_400_BAD_REQUEST)
        if triwulan is None or triwulan == '':
            data['status'] = False
            data['message'] = 'Parameter triwulan harus disertakan'
            return Response(data, status = status.HTTP_400_BAD_REQUEST)

        kwargs = {}
        kwargs['tahun'] = tahun
        kwargs['bulan__in'] = TRIWULAN[triwulan]
        if jenis == 'pendapatan':
            kwargs['kode_rekening__startswith'] = 4
        elif jenis == 'belanja':
            kwargs['kode_rekening__startswith'] = 5
        elif jenis == 'pembiayaan':
            kwargs['kode_rekening__startswith'] = 6
        elif jenis == 'silpa':
            kwargs['kode_rekening__icontains'] = 'SISA LEBIH'
        else:
            data['status'] = False
            data['message'] = 'Jenis tidak ditemukan.'
            return Response(data, status = status.HTTP_400_BAD_REQUEST)            
        
        dict_1 = {}

        data_pendapatan = T_LRA_Rekening.objects.filter(**kwargs).annotate(total_pendapatan_sebelum_pergeseran = Sum('anggaran_sebelum_pergeseran'),
                                                                                            total_pendapatan_setelah_pergeseran = Sum('anggaran_setelah_pergeseran')).order_by('kode_rekening')
        total_pendapatan_sebelum_pergeseran = 0
        total_pendapatan_setelah_pergeseran = 0
        selisih = 0
        realisasi = 0
        presentase = 0
        sisa = 0
        
        total_pendapatan_sebelum_pergeseran1 = 0
        total_pendapatan_setelah_pergeseran1 = 0
        selisih1 = 0
        realisasi1 = 0
        presentase1 = 0
        sisa1 = 0

        total_pendapatan_sebelum_pergeseran2 = 0
        total_pendapatan_setelah_pergeseran2 = 0
        selisih2 = 0
        realisasi2 = 0
        presentase2 = 0
        sisa2 = 0

        total_pendapatan_sebelum_pergeseran3 = 0
        total_pendapatan_setelah_pergeseran3 = 0
        selisih3 = 0
        realisasi3 = 0
        presentase3 = 0
        sisa3 = 0

        total_pendapatan_sebelum_pergeseran4 = 0
        total_pendapatan_setelah_pergeseran4 = 0
        selisih4 = 0
        realisasi4 = 0
        presentase4 = 0
        sisa4 = 0
        
        for x in data_pendapatan:
            if jenis != 'silpa':
                if len(x.kode_rekening.replace(' ', '').split('.')) == 1:
                    dict_1['level'] = 'akun'
                    dict_1['kode_rekening'] = x.kode_rekening.replace(' ', '')
                    dict_1['kodeakun'] = x.kode_rekening.replace(' ', '').split('.')[0]
                    dict_1['uraian'] = x.uraian
                    dict_1['detail'] = []
                
                if len(x.kode_rekening.replace(' ', '').split('.')) == 2:
                    total_pendapatan_sebelum_pergeseran += x.total_pendapatan_sebelum_pergeseran
                    total_pendapatan_setelah_pergeseran += x.total_pendapatan_setelah_pergeseran
                    selisih += x.selisih
                    realisasi += x.realisasi
                    presentase = round((realisasi / total_pendapatan_setelah_pergeseran) * 100, 2)
                    sisa += x.sisa
                    dict_1['selisih'] = selisih
                    dict_1['realisasi'] = realisasi
                    dict_1['presentase'] = presentase
                    dict_1['sisa'] = sisa
                    dict_1['total_sebelum_pergeseran'] = total_pendapatan_sebelum_pergeseran
                    dict_1['total_setelah_pergeseran'] = total_pendapatan_setelah_pergeseran

                    dict_2 = {}
                    dict_2['level'] = 'kelompok'
                    dict_2['kode_rekening'] = x.kode_rekening.replace(' ', '')
                    dict_2['kodeakun'] = x.kode_rekening.replace(' ', '').split('.')[0]
                    dict_2['kodekelompok'] = x.kode_rekening.replace(' ', '').split('.')[1]
                    dict_2['uraian'] = x.uraian
                    dict_2['detail'] = []

                    dict_1['detail'].append(dict_2)
                    
                if len(x.kode_rekening.replace(' ', '').split('.')) == 3:
                    total_pendapatan_sebelum_pergeseran1 += x.total_pendapatan_sebelum_pergeseran
                    total_pendapatan_setelah_pergeseran1 += x.total_pendapatan_setelah_pergeseran
                    dict_2['total_sebelum_pergeseran'] = total_pendapatan_sebelum_pergeseran1
                    dict_2['total_setelah_pergeseran'] = total_pendapatan_setelah_pergeseran1

                    selisih1 += x.selisih
                    realisasi1 += x.realisasi
                    presentase1 = round((realisasi1 / total_pendapatan_setelah_pergeseran1) * 100, 2)
                    sisa1 += x.sisa
                    dict_2['selisih'] = selisih1
                    dict_2['realisasi'] = realisasi1
                    dict_2['presentase'] = presentase1
                    dict_2['sisa'] = sisa1

                    dict_3 = {}
                    dict_3['level'] = 'jenis'
                    dict_3['kode_rekening'] = x.kode_rekening.replace(' ', '')
                    dict_3['kodeakun'] = x.kode_rekening.replace(' ', '').split('.')[0]
                    dict_3['kodekelompok'] = x.kode_rekening.replace(' ', '').split('.')[1]
                    dict_3['kodejenis'] = x.kode_rekening.replace(' ', '').split('.')[2]
                    dict_3['uraian'] = x.uraian
                    dict_3['detail'] = []

                    dict_2['detail'].append(dict_3)

                if len(x.kode_rekening.replace(' ', '').split('.')) == 4:
                    total_pendapatan_sebelum_pergeseran2 += x.total_pendapatan_sebelum_pergeseran
                    total_pendapatan_setelah_pergeseran2 += x.total_pendapatan_setelah_pergeseran
                    dict_3['total_sebelum_pergeseran'] = total_pendapatan_sebelum_pergeseran2
                    dict_3['total_setelah_pergeseran'] = total_pendapatan_setelah_pergeseran2

                    selisih2 += x.selisih
                    realisasi2 += x.realisasi
                    presentase2 = round((realisasi2 / total_pendapatan_setelah_pergeseran2) * 100, 2)
                    sisa2 += x.sisa
                    dict_3['selisih'] = selisih2
                    dict_3['realisasi'] = realisasi2
                    dict_3['presentase'] = presentase2
                    dict_3['sisa'] = sisa2
                    dict_4 = {}
                    dict_4['level'] = 'objek'
                    dict_4['kode_rekening'] = x.kode_rekening.replace(' ', '')
                    dict_4['kodeakun'] = x.kode_rekening.replace(' ', '').split('.')[0]
                    dict_4['kodekelompok'] = x.kode_rekening.replace(' ', '').split('.')[1]
                    dict_4['kodejenis'] = x.kode_rekening.replace(' ', '').split('.')[2]
                    dict_4['kodeobjek'] = x.kode_rekening.replace(' ', '').split('.')[3]
                    dict_4['uraian'] = x.uraian
                    dict_4['detail'] = []

                    dict_3['detail'].append(dict_4)

                if len(x.kode_rekening.replace(' ', '').split('.')) == 5:
                    total_pendapatan_sebelum_pergeseran3 += x.total_pendapatan_sebelum_pergeseran
                    total_pendapatan_setelah_pergeseran3 += x.total_pendapatan_setelah_pergeseran
                    dict_4['total_sebelum_pergeseran'] = total_pendapatan_sebelum_pergeseran3
                    dict_4['total_setelah_pergeseran'] = total_pendapatan_setelah_pergeseran3

                    selisih3 += x.selisih
                    realisasi3 += x.realisasi
                    presentase3 = round((realisasi3 / total_pendapatan_setelah_pergeseran3) * 100, 2)
                    sisa3 += x.sisa
                    dict_4['selisih'] = selisih3
                    dict_4['realisasi'] = realisasi3
                    dict_4['presentase'] = presentase3
                    dict_4['sisa'] = sisa3

                    dict_5 = {}
                    dict_5['level'] = 'rincianobjek'
                    dict_5['kode_rekening'] = x.kode_rekening.replace(' ', '')
                    dict_5['kodeakun'] = x.kode_rekening.replace(' ', '').split('.')[0]
                    dict_5['kodekelompok'] = x.kode_rekening.replace(' ', '').split('.')[1]
                    dict_5['kodejenis'] = x.kode_rekening.replace(' ', '').split('.')[2]
                    dict_5['kodeobjek'] = x.kode_rekening.replace(' ', '').split('.')[3]
                    dict_5['koderincianobjek'] = x.kode_rekening.replace(' ', '').split('.')[4]
                    dict_5['uraian'] = x.uraian
                    dict_5['detail'] = []

                    dict_4['detail'].append(dict_5)

                if len(x.kode_rekening.replace(' ', '').split('.')) == 6:
                    total_pendapatan_sebelum_pergeseran4 += x.total_pendapatan_sebelum_pergeseran
                    total_pendapatan_setelah_pergeseran4 += x.total_pendapatan_setelah_pergeseran
                    dict_5['total_sebelum_pergeseran'] = total_pendapatan_sebelum_pergeseran4
                    dict_5['total_setelah_pergeseran'] = total_pendapatan_setelah_pergeseran4

                    selisih4 += x.selisih
                    realisasi4 += x.realisasi
                    presentase4 = round((realisasi4 / total_pendapatan_setelah_pergeseran4) * 100, 2)
                    sisa4 += x.sisa
                    dict_5['selisih'] = selisih4
                    dict_5['realisasi'] = realisasi4
                    dict_5['presentase'] = presentase4
                    dict_5['sisa'] = sisa4

                    dict_6 = {}
                    dict_6['level'] = 'subrincianobjek'
                    dict_6['kode_rekening'] = x.kode_rekening.replace(' ', '')
                    dict_6['kodeakun'] = x.kode_rekening.replace(' ', '').split('.')[0]
                    dict_6['kodekelompok'] = x.kode_rekening.replace(' ', '').split('.')[1]
                    dict_6['kodejenis'] = x.kode_rekening.replace(' ', '').split('.')[2]
                    dict_6['kodeobjek'] = x.kode_rekening.replace(' ', '').split('.')[3]
                    dict_6['koderincianobjek'] = x.kode_rekening.replace(' ', '').split('.')[4]
                    dict_6['kodesubrincianobjek'] = x.kode_rekening.replace(' ', '').split('.')[5]
                    dict_6['uraian'] = x.uraian
                    dict_6['detail'] = []

                    dict_5['detail'].append(dict_6)
            else:
                dict_1['uraian'] = x.uraian
                dict_1['selisih'] = x.selisih
                dict_1['realisasi'] = x.realisasi
                try:
                    dict_1['presentase'] = round((x.realisasi / x.total_pendapatan_setelah_pergeseran) * 100, 2)
                except Exception as e:
                    dict_1['presentase'] = 100
                dict_1['sisa'] = x.sisa

        
        data['data'] = dict_1

        return Response(data)