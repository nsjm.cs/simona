# Generated by Django 3.2 on 2023-10-14 08:13

from django.db import migrations, models
import django.utils.timezone
import rizq_sanjaya_app.models.profile


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('username', models.CharField(max_length=50, unique=True)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('is_staff', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('is_superuser', models.BooleanField(default=False)),
                ('is_verified', models.BooleanField(default=False)),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now)),
                ('last_login', models.DateTimeField(null=True)),
                ('phone', models.CharField(max_length=15)),
                ('date_of_birth', models.DateField(blank=True, null=True)),
                ('avatar', models.ImageField(blank=True, null=True, upload_to='profile/images/avatar/', validators=[rizq_sanjaya_app.models.profile.validate_file_gambar, rizq_sanjaya_app.models.profile.validate_file_size_gambar])),
                ('role', models.CharField(choices=[('admin', 'Admin'), ('posting', 'Posting'), ('pejabat', 'pejabat')], default='pejabat', max_length=50)),
                ('email_verification_token', models.CharField(default='', max_length=100)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('objects', rizq_sanjaya_app.models.profile.AccountManager()),
            ],
        ),
        migrations.CreateModel(
            name='master_pejabat_pengesah',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nip', models.TextField(null=True)),
                ('nama_pejabat', models.TextField(null=True)),
                ('jabatan', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='T_LRA_Jenis',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tahun', models.TextField(null=True)),
                ('kode_rekening', models.TextField(null=True)),
                ('uraian', models.TextField(null=True)),
                ('anggaran_sebelum_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('anggaran_setelah_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('selisih', models.DecimalField(decimal_places=2, max_digits=17)),
                ('realisasi', models.DecimalField(decimal_places=2, max_digits=17)),
                ('persentase', models.TextField(null=True)),
                ('sisa', models.DecimalField(decimal_places=2, max_digits=17)),
                ('bulan', models.TextField(null=True)),
                ('status', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='T_LRA_Objek',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tahun', models.TextField(null=True)),
                ('kode_rekening', models.TextField(null=True)),
                ('uraian', models.TextField(null=True)),
                ('anggaran_sebelum_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('anggaran_setelah_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('selisih', models.DecimalField(decimal_places=2, max_digits=17)),
                ('realisasi', models.DecimalField(decimal_places=2, max_digits=17)),
                ('persentase', models.TextField(null=True)),
                ('sisa', models.DecimalField(decimal_places=2, max_digits=17)),
                ('keterangan', models.TextField(null=True)),
                ('bulan', models.TextField(null=True)),
                ('status', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='T_LRA_Rekening',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tahun', models.TextField(null=True)),
                ('kode_rekening', models.TextField(null=True)),
                ('uraian', models.TextField(null=True)),
                ('anggaran_sebelum_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('anggaran_setelah_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('selisih', models.DecimalField(decimal_places=2, max_digits=17)),
                ('realisasi', models.DecimalField(decimal_places=2, max_digits=17)),
                ('persentase', models.TextField(null=True)),
                ('sisa', models.DecimalField(decimal_places=2, max_digits=17)),
                ('bulan', models.TextField(null=True)),
                ('status', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='T_OPD_Peringkat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tahun', models.TextField(null=True)),
                ('kode_rekening', models.TextField(null=True)),
                ('uraian', models.TextField(null=True)),
                ('anggaran_setelah_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('realisasi', models.DecimalField(decimal_places=2, max_digits=17)),
                ('persentase', models.TextField(null=True)),
                ('sisa', models.DecimalField(decimal_places=2, max_digits=17)),
                ('keterangan', models.TextField(null=True)),
                ('bulan', models.TextField(null=True)),
                ('status', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='T_Real_Rekening',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tahun', models.TextField(null=True)),
                ('kode_rekening', models.TextField(null=True)),
                ('uraian', models.TextField(null=True)),
                ('anggaran_sebelum_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('anggaran_setelah_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('selisih', models.DecimalField(decimal_places=2, max_digits=17)),
                ('realisasi', models.DecimalField(decimal_places=2, max_digits=17)),
                ('persentase', models.TextField(null=True)),
                ('sisa', models.DecimalField(decimal_places=2, max_digits=17)),
                ('keterangan', models.TextField(null=True)),
                ('bulan', models.TextField(null=True)),
                ('status', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='T_Real_Sub_Kegiatan',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tahun', models.TextField(null=True)),
                ('kode_rekening', models.TextField(null=True)),
                ('uraian', models.TextField(null=True)),
                ('anggaran_sebelum_pergeseran', models.DecimalField(decimal_places=2, max_digits=17)),
                ('selisih', models.DecimalField(decimal_places=2, max_digits=17)),
                ('realisasi', models.DecimalField(decimal_places=2, max_digits=17)),
                ('persentase', models.TextField(null=True)),
                ('sisa', models.DecimalField(decimal_places=2, max_digits=17)),
                ('keterangan', models.TextField(null=True)),
                ('bulan', models.TextField(null=True)),
                ('status', models.TextField(null=True)),
            ],
        ),
    ]
