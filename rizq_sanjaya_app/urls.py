from django.urls import path, include
from .views import *
from django.contrib.auth import views as auth_views


app_name = 'profile'
urlpatterns = [
    # PATH URL UNTUK ADMIN 
    path('manage_panel/', include(
        [
            path('', admin.index, name='admin_home'),
            path('verification/', user.verification, name='admin_verification'),
            path('send_verification/', user.send_verification, name='admin_send_verification'),
            path('email/verify/<uidb64>/<token>/',user.email_verify, name='admin_email_verify'),
            # LRA JENIS =====================================
            path('lra_jenis/', include(
            [
                path('', lra_jenis.admin_index, name='admin_lra_jenis'),
            ]
            )),

            # LRA objek =====================================
            path('lra_objek/', include(
            [
                path('', lra_objek.admin_index, name='admin_lra_objek'),
            ]
            )),

            # LRA Rekening =====================================
            path('lra_rekening/', include(
            [
                path('', lra_rekening.admin_index, name='admin_lra_rekening'),
            ]
            )),

            # OPD Peringkat =====================================
            path('opd_peringkat/', include(
            [
                path('', opd_peringkat.admin_index, name='admin_opd_peringkat'),
            ]
            )),

            # Real Rekening =====================================
            path('lra_real_rekening/', include(
            [
                path('', lra_real_rekening.admin_index, name='admin_lra_real_rekening'),
            ]
            )),
             # OPD Peringkat =====================================
            path('pejabat_pengesah/', include(
            [
                path('', pejabat_pengesah.admin_index, name='admin_pejabat_pengesah'),
            ]
            )),


            # USERS
            path('user/', include(
                [
                    path('', user.admin_index, name='admin_user'),
                    path('create/', user.create, name='admin_user_create'),
                    path('password/edit/', user.edit_password, name='admin_user_password_edit'),
                    path('profile/edit/<str:username>', user.edit_profile, name='admin_user_edit'),
                    path('avatar/edit/<str:username>', user.edit_avatar, name='admin_user_avatar_edit'),
                    path('detail/', user.admin_detail, name='admin_user_detail'),
                    path('soft_delete/<str:username>', user.softDelete, name='admin_user_soft_delete'),
                    path('permanent_delete/<str:slug>', user.permanentDelete, name='admin_user_permanent_delete'),
                    path('restore/<str:username>', user.restore, name='admin_user_restore'),
                    path('edit_role/', user.edit_role, name='admin_user_edit_role'),
                    path('non_aktif/<int:id>', user.non_aktif, name='non_aktif'),
                    path('re_aktif/<int:id>', user.re_aktif, name='aktif'),
                ]
            )),

            # SETTING APLIKASI
            path('setting/', include(
                [
                    # path('', user.admin_index, name='admin_setting'),
                    path('infografis/', setting.admin_index_infografis, name='admin_setting_infografis'),
                   
                ]
            )),
        ]
    ))
]