from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from ..models import News
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db import connection
from django.db.models import Q, Count
import os
from django.template.loader import render_to_string
import requests



@require_http_methods(["GET"])
def index(request):
     title = 'Sistem Informasi Monotoring Data Anggaran Provinsi Papua Tengah'
     description = 'Aplikasi ini dibuat untuk memonitor data pergerakan anggaran dan realisasi secara realtime dan memberikan kemudahan dalam penyejian data pelaporan kepada eksekutif / pimpinan daerah'
     dataLanding = {
          'Title' : title,
          'Description' : description,
     }
     
     return render(request, 'frontend/index.html',dataLanding)



