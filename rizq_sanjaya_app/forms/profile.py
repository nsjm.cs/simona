from django import forms
from ..models import *
from PIL import Image
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from captcha.fields import CaptchaField

# class CategoryForm(forms.ModelForm):
# 	class Meta:
# 		model = Category
# 		fields = ('id', 'nama','typexs',)

class AvatarForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = Account
		fields = ('avatar', 'x', 'y', 'width', 'height',)

	def save(self):
		account = super(AvatarForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(account.avatar)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{account.avatar.name}")

		return account

class AccountForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('first_name', 'last_name', 'phone', 'date_of_birth')

class RoleForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('role',)

class CustomUserCreationForm(UserCreationForm):
	class Meta:
		model = Account
		fields = ('email', 'username', 'first_name', 'last_name', 'role', 'phone', 'date_of_birth',)

