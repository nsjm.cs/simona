// replace the textarea with CKEditor instance
CKEDITOR.replace('description');

// add event listener to form submit
document.querySelector('form').addEventListener('submit', function(event) {
    // get the data from CKEditor instance and update the textarea
    var description = CKEDITOR.instances.description.getData();
    document.querySelector('#description').value = description;
});
