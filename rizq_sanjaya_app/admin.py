from django.contrib import admin
from .models import *

"""ACCOUNT USER"""
class UserAdmin(admin.ModelAdmin):
    list_display = ("email", "username", "role", "is_active", "is_verified", "is_staff")
admin.site.register(Account, UserAdmin)
