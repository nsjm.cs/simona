from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from ..decorators import *
from django.db.models.functions import Substr, StrIndex

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    context = {
        'title' : 'Monitoring LRA Rekening',
    }
    
    return render(request, 'profile/admin/lra_rekening/index.html', context)