from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_list_berita, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    data_master_pejabat = master_pejabat_pengesah.objects.all()
    archives = master_pejabat_pengesah.objects.all()
    paginator = Paginator(data_master_pejabat, 15)
    try:
        data_master_pejabat = paginator.page(page)
    except PageNotAnInteger:
        data_master_pejabat = paginator.page(1)
    except EmptyPage:
        data_master_pejabat = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Master Pejabat Pengesah',
        'data_master_pejabat' : data_master_pejabat,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/pejabat_pengesah/index.html', context)

# @login_required
# @is_verified()
# def create(request):
#     template = ''
#     context = {} 
#     form = ''

#     if request.method == 'GET':
#         form = Kata.objects.all()
#         context = {
#             'title' : 'ADD KATA MEREKA',
#             'form' : form,
#         }

#         template = 'profile/admin/kata/create.html'
#         return render(request, template, context)
    
#     if request.method == 'POST':
#         image = request.FILES['image']
#         judul = request.POST.get('judul')
#         keterangan = request.POST.get('keterangan')
#         if judul is not None:
#             new_news = Kata()
#             new_news.image = image
#             new_news.judul = judul
#             new_news.keterangan = keterangan
#             new_news.save()
            
#             messages.success(request, 'Kata Mereka berhasil disimpan.')
#             return redirect('profile:admin_KM')

#         messages.error(request, 'Kata Mereka gagal disimpan.')
#         return render(request, 'profile/admin/kata/create.html', {'form': form,})
    
# @login_required
# @is_verified()
# def edit(request, id):
#     template = ''
#     context = {} 
#     form = ''
    
#     if request.method == 'GET':
#         news = Kata.objects.get(id = id)

#         context = {
#             'title' : 'EDIT KATA MEREKA',
#             'form' : form,
#             'edit' : 'true',
#             'news' : news,
#         }
#         template = 'profile/admin/kata/create.html'
#         return render(request, template, context)
    
#     if request.method == 'POST':
#         news = Kata.objects.get(id = id)
#         path_file_lama = f"{settings.MEDIA_ROOT}/{news.image}"
#         foto_old = bool(news.image)
#         judul = request.POST.get('judul')
#         keterangan = request.POST.get('keterangan')

#         if news is not None:
#             news.judul = judul
#             news.keterangan = keterangan
#             news.save()
    
#             if 'image' in request.FILES:
#                 if foto_old : 
#                     try:
#                         os.remove(path_file_lama)
#                     except:
#                         pass
#                 foto = request.FILES['image']
#                 news.image = foto
#                 news.save()

#             messages.success(request, 'Kata Mereka berhasil disimpan')
#             return redirect('profile:admin_KM')

#         messages.error(request, 'Kata Mereka gagal disimpan.')
#         return render(request, 'profile/admin/kata/create.html', {'form': form,})

# @login_required
# @is_verified()
# @require_http_methods(["GET"])
# def admin_detail(request, id):
#     template = 'profile/admin/kata/detail.html'
#     try:
#         news = Kata.objects.get(id=id)
#     except Kata.DoesNotExist:
#         news = None
#     context = {
#         'title' : news.judul if news != None else 'TIDAK DITEMUKAN',
#         'news' : news,
#     }
#     return render(request, template, context)

# @login_required
# @is_verified()
# def softDelete(request, id):
#     message = ''
#     try:
#         sekarang = timezone.now()
#         doc = Kata.objects.get(id=id)
#         doc.deleted_at = sekarang
#         doc.save()
#         message = 'success'
#     except Kata.DoesNotExist:
#         message = 'error'

#     context = {
#             'message' : message,
#         }

#     return HttpResponse(context)

# @login_required
# @is_verified()
# def permanentDelete(request, id):
#     message = ''
#     try:
#         doc = Kata.objects.get(id=id)
#         try:
#             doc.image.delete()
#         except:
#             pass
#         doc.delete()
#         message = 'success'
#     except Kata.DoesNotExist:
#         message = 'error'

#     context = {
#             'message' : message,
#         }

#     return HttpResponse(context)
    
# @login_required
# @is_verified()
# def restore(request, id):
#     message = ''
#     try:
#         doc = Kata.objects.get(id=id)
#         doc.deleted_at = None
#         doc.save()
#         message = 'success'
#     except Kata.DoesNotExist:
#         message = 'error'

#     context = {
#             'message' : message,
#         }

#     return HttpResponse(context)