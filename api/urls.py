from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from api.views import basic_views


app_name = 'api_url'
urlpatterns = [
    path('get_data/<str:jenis>/', basic_views.GetDataViewsets.as_view()),
    # path('getaboutme/<str:section>/', basic_views.AboutMeViewsets.as_view()),
    # path('getlayanan/', basic_views.LayananViewsets.as_view()),
    # path('getrecommend_produk/<str:jenis>/<int:len_data>/', basic_views.ProdukRecommendViewsets.as_view()),
    # path('getallproduct/<str:jenis>/', basic_views.AllProdukViewsets.as_view()),
    # path('getproduct/<str:idproduct>/', basic_views.ProductViewset.as_view()),
    # path('orderdetail/', basic_views.OrderDetail.as_view(), name = 'orderdetail'),
    # path('sendotp/', basic_views.SendOTP.as_view(), name = 'sendotp'),
    # path('verifyotp/', basic_views.VerifyOTP.as_view(), name = 'verifyotp'),
    # path('sendemailpesananclient/', basic_views.SendEmailPesananClient.as_view(), name = 'sendemailpesananclient'),
    # path('sendemailpesananadmin/', basic_views.SendEmailPesananAdmin.as_view(), name = 'sendemailpesananadmin'),
    # path('get_all_order/', basic_views.GetAllOrder.as_view(), name = 'get_all_order'),
    # path('kirim_bukti_bayar/', basic_views.KirimBuktiBayar.as_view(), name = 'kirim_bukti_bayar'),
    # path('batalkan_pesanan/', basic_views.BatalkanPesanan.as_view(), name = 'batalkan_pesanan'),
    # path('productcategory/', basic_views.CategoryViewsets.as_view()),
    # path('productcategorydetail/<slug:slug>/', basic_views.CategoryDetailViewsets.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)