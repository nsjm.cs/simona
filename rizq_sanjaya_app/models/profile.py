from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.text import slugify
import string
import random

from requests import request
from ckeditor_uploader.fields import RichTextUploadingField
from taggit.managers import TaggableManager
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.auth import get_user_model
from django.utils import timezone
import datetime
from ckeditor.fields import RichTextField
import uuid

""""==============================VALIDATOR======================================"""
def validate_file_pdf(value):
	pass

"""VALIDASI UNTUK FILE DOKUMEN"""
def validate_file_dokumen(value):
	dokumen =[
			# 'application/vnd.ms-excel', 
			# 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 
			'application/pdf', 
			'image/jpeg',
			'image/png',
			# 'text/csv',
			# 'application/msword',
			# 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			]
	if value.file.content_type not in dokumen:
		# raise ValidationError(u'Pastikan ekstensi file adalah .csv, .doc, .docx, .pdf, .xls atau .xlxs.')
		raise ValidationError(u'Pastikan ekstensi file adalah .jpeg, .png, .pdf')

"""VALIDASI UNTUK FILE GAMBAR"""
def validate_file_gambar(value):
	dokumen =[
				'image/jpeg',
				'image/png',
				'image/webp',
				'image/jpg',
			]
	if value.file.content_type not in dokumen:
		raise ValidationError(u'Pastikan ekstensi file adalah .jpg, .jpeg atau .png.')

"""VALIDASI UNTUK UKURAN MAKSIMAL UKURAN FILE"""
def validate_file_size_dokumen(value):
	filesize= value.size
	
	# if filesize > 5242880:
	if filesize > 5242880:
		raise ValidationError("Pastikan ukuran File dibawah 5 MB.")
	else:
		return value

"""VALIDASI UNTUK UKURAN MAKSIMAL UKURAN GAMBAR"""
def validate_file_size_gambar(value):
	filesize= value.size
	
	# if filesize > 5242880:
	if filesize > 2242880:
		raise ValidationError("Pastikan ukuran File dibawah 2 MB.")
	else:
		return value


""""==============================FUNGSI======================================"""
"""MEMBERIKAN NILAI RANDOM UNTUK SLUG KALO DUPLIKAT"""
def rand_slug():
	rand = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))
	return rand.lower()

COLOR_CHOICES = [
	('#0d6efd', 'Biru'),
	('#6610f2', 'Indigo'),
	('#6f42c1', 'Ungu'),
	('#d63384', 'Merah Muda'),
	('#dc3545', 'Merah'),
	('#fd7e14', 'Oranye'),
	('#ffc107', 'Kuning'),
	('#198754', 'Hijau'),
	('#20c997', 'Teal'),
	('#0dcaf0', 'Cyan'),
	('#adb5bd', 'Abu-Abu'),
	('#000', 'Hitam'),
]
ANNOUNCEMENT_CHOICES = [
	('ASN', 'ASN'),
	('Umum', 'Umum'),
]
ROLE_CHOICES = [
	('admin', 'Admin'),
	('posting', 'Posting'),
	('pejabat', 'pejabat'),
]


""""==============================MODEL TABEL======================================"""
"""BASE MODEL BIAR GA NGULANG2 MASUKIN 3 FIELD KETERANGAN WAKTU INI"""
class Time(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	deleted_at = models.DateTimeField(null=True, blank=True)

	class Meta:
		managed  = False 
		abstract = True
		
class AccountManager(BaseUserManager):
	use_in_migrations = True

	def _create_user(self, email, username, phone, password, **extra_fields):
		values = [email, username, phone,]
		field_value_map = dict(zip(self.model.REQUIRED_FIELDS, values))
		for field_name, value in field_value_map.items():
			if not value:
				raise ValueError('The {} value must be set'.format(field_name))

		email = self.normalize_email(email)
		user = self.model(
			email=email,
			username=username,
			phone=phone,
			**extra_fields
		)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_user(self, email, username, phone, password=None, **extra_fields):
		extra_fields.setdefault('is_staff', False)
		extra_fields.setdefault('is_superuser', False)
		return self._create_user(email, username, phone, password, **extra_fields)

	def create_superuser(self, email, username, phone, password=None, **extra_fields):
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_superuser', True)
		extra_fields.setdefault('is_verified', True)

		if extra_fields.get('is_staff') is not True:
			raise ValueError('Superuser must have is_staff=True.')
		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')
		if extra_fields.get('is_verified') is not True:
			raise ValueError('Superuser must have is_verified=True.')

		return self._create_user(email, username, phone, password, **extra_fields)


"""TABEL AKUN UNTUK SELAIN BAWAANNYA DJANGO YANG DIPAKAI"""
class Account(AbstractBaseUser):
	email = models.EmailField(unique=True)
	username = models.CharField(unique=True, max_length=50)
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	is_staff = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	is_superuser = models.BooleanField(default=False)
	is_verified = models.BooleanField(default=False)
	date_joined = models.DateTimeField(default=timezone.now)
	last_login = models.DateTimeField(null=True)
	phone = models.CharField(max_length=15)
	date_of_birth = models.DateField(blank=True, null=True)
	avatar = models.ImageField(blank=True, null=True, upload_to='profile/images/avatar/', validators=[validate_file_gambar, validate_file_size_gambar],)
	role = models.CharField(max_length=50, choices=ROLE_CHOICES, default='pejabat')
	email_verification_token = models.CharField(max_length=100, default='')
	
	objects = AccountManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['username', 'phone', 'role']

	def get_full_name(self):
		return f"{self.first_name} {self.last_name}"

	def get_short_name(self):
		return self.first_name

""""==============================SIMONA DB======================================"""

class master_pejabat_pengesah(models.Model):
	nip = models.TextField(null=True)
	nama_pejabat = models.TextField(null=True)
	jabatan = models.TextField(null=True)

class T_LRA_Objek(models.Model):
	tahun = models.CharField(max_length = 4, null=True)
	kode_rekening = models.TextField(null=True)
	uraian = models.TextField(null=True)
	anggaran_sebelum_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	anggaran_setelah_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	selisih = models.DecimalField(max_digits=17, decimal_places=2)
	realisasi = models.DecimalField(max_digits=17, decimal_places=2)
	persentase = models.TextField(null=True)
	sisa = models.DecimalField(max_digits=17, decimal_places=2)
	keterangan = models.TextField(null=True)
	bulan = models.IntegerField(null=True)
	status = models.TextField(null=True)
	
class T_LRA_Jenis(models.Model):
	tahun = models.CharField(max_length = 4, null=True)
	kode_rekening = models.TextField(null=True)
	uraian = models.TextField(null=True)
	anggaran_sebelum_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	anggaran_setelah_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	selisih = models.DecimalField(max_digits=17, decimal_places=2)
	realisasi = models.DecimalField(max_digits=17, decimal_places=2)
	persentase = models.TextField(null=True)
	sisa = models.DecimalField(max_digits=17, decimal_places=2)
	bulan = models.IntegerField(null=True)
	status = models.TextField(null=True)
	
class T_LRA_Rekening(models.Model):
	tahun = models.CharField(max_length = 4, null=True)
	kode_rekening = models.TextField(null=True)
	uraian = models.TextField(null=True)
	anggaran_sebelum_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	anggaran_setelah_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	selisih = models.DecimalField(max_digits=17, decimal_places=2)
	realisasi = models.DecimalField(max_digits=17, decimal_places=2)
	persentase = models.TextField(null=True)
	sisa = models.DecimalField(max_digits=17, decimal_places=2)
	bulan = models.IntegerField(null=True)
	status = models.TextField(null=True)
	
class T_OPD_Peringkat(models.Model):
	tahun = models.CharField(max_length = 4, null=True)
	kode_rekening = models.TextField(null=True)
	uraian = models.TextField(null=True)
	anggaran_setelah_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	realisasi = models.DecimalField(max_digits=17, decimal_places=2)
	persentase = models.TextField(null=True)
	sisa = models.DecimalField(max_digits=17, decimal_places=2)
	keterangan = models.TextField(null=True)
	bulan = models.IntegerField(null=True)
	status = models.TextField(null=True)
	
class T_Real_Sub_Kegiatan(models.Model):
	tahun = models.CharField(max_length = 4, null=True)
	kode_rekening = models.TextField(null=True)
	uraian = models.TextField(null=True)
	anggaran_sebelum_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	selisih = models.DecimalField(max_digits=17, decimal_places=2)
	realisasi = models.DecimalField(max_digits=17, decimal_places=2)
	persentase = models.TextField(null=True)
	sisa = models.DecimalField(max_digits=17, decimal_places=2)
	keterangan = models.TextField(null=True)
	bulan = models.IntegerField(null=True)
	status = models.TextField(null=True)
	
class T_Real_Rekening(models.Model):
	tahun = models.CharField(max_length = 4, null=True)
	kode_rekening = models.TextField(null=True)
	uraian = models.TextField(null=True)
	anggaran_sebelum_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	anggaran_setelah_pergeseran = models.DecimalField(max_digits=17, decimal_places=2)
	selisih = models.DecimalField(max_digits=17, decimal_places=2)
	realisasi = models.DecimalField(max_digits=17, decimal_places=2)
	persentase = models.TextField(null=True)
	sisa = models.DecimalField(max_digits=17, decimal_places=2)
	keterangan = models.TextField(null=True)
	bulan = models.IntegerField(null=True)
	status = models.TextField(null=True)
	
