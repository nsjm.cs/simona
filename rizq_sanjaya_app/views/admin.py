from django.shortcuts import render, HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from rizq_sanjaya_app.decorators import is_verified
from ..forms import *
from ..models import *
from django.db import connections
from ..helpers import dictfetchall

@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    halo = 'welcome'
    context = {
        'Welcome' : halo,
        
    }
    return render(request, 'profile/admin/index.html', context)