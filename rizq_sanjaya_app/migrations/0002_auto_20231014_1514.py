# Generated by Django 3.2 on 2023-10-14 08:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rizq_sanjaya_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='t_lra_jenis',
            name='bulan',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='t_lra_objek',
            name='bulan',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='t_lra_rekening',
            name='bulan',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='t_opd_peringkat',
            name='bulan',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='t_real_rekening',
            name='bulan',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='t_real_sub_kegiatan',
            name='bulan',
            field=models.IntegerField(null=True),
        ),
    ]
